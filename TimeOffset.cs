﻿using System;
using System.IO;
using System.Globalization;

namespace SampleCode
{
    public struct TimeOffset : IComparable, IComparable<TimeOffset>
    {
        public static readonly TimeOffset MaxValue = new(long.MaxValue);
        public static readonly TimeOffset MinValue = new(0);

        public long ms;

        public TimeOffset(long milliseconds)
        {
            ms = milliseconds;
        }

        public TimeOffset(DateTimeOffset ts) : this(ts.ToUnixTimeMilliseconds()) { }
        public TimeOffset(DateTime ts) : this((DateTimeOffset)(DateTime.SpecifyKind(ts, DateTimeKind.Utc))) { }
        public static TimeOffset Parse(string s)
            => new(DateTime.Parse(s, CultureInfo.InvariantCulture, DateTimeStyles.AdjustToUniversal));
        public readonly TimeOffset FloorToSecond() => new(ms - ms % 1000);
        public readonly TimeOffset FloorToMinute() => new(ms - ms % 60_000);
        public readonly TimeOffset FloorToHour() => new(ms - ms % 3600_000);
        public readonly TimeOffset FloorToDay() => new(ms - ms % 86_400_000);

        public readonly TimeOffset Date => FloorToDay();

        public readonly TimeOffset AddMilliseconds(int milliseconds) => new(ms + milliseconds);
        public readonly TimeOffset AddSeconds(int seconds) => new(ms + 1000 * (long)seconds);
        public readonly TimeOffset AddMinutes(int minutes) => new(ms + 60_000 * (long)minutes);
        public readonly TimeOffset AddHours(int hours) => new(ms + 3600_000 * (long)hours);
        public readonly TimeOffset AddDays(int days) => new(ms + 86_400_000 * (long)days);
        public readonly DateTimeOffset ToDateTimeOffset() => DateTimeOffset.FromUnixTimeMilliseconds(ms);
        public readonly DateTime ToDateTime() => ToDateTimeOffset().DateTime;
        public readonly int TotalSeconds() => (int)((ms + 999) / 1000);
        public readonly int TotalMinutes() => MinutesFromMilliseconds(ms);
        public readonly int CompareTo(object? obj)
            => obj switch
            {
                TimeOffset other => CompareTo(other),
                null => throw new ArgumentNullException(nameof(obj), "Object for compare are null"),
                _ => throw new ArgumentNullException(nameof(obj), $"Object is not {nameof(TimeOffset)}")
            };
        public readonly int CompareTo(TimeOffset other)
            => ms.CompareTo(other.ms);

        public static int MinutesFromMilliseconds(long ms) => (int)((ms + 59999) / 60000);
        public static TimeOffset UtcNow => new(DateTimeOffset.UtcNow);
        public static TimeOffset FromSeconds(double sec) => new(Convert.ToInt64(sec * 1000));
        public static TimeOffset FromMinutes(double minutes) => new(Convert.ToInt64(minutes * 60000));
        public static int Compare(TimeOffset a, TimeOffset b) => a.ms.CompareTo(b.ms);

        public static TimeOffset operator +(TimeOffset a, long b) => new(a.ms + b);
        public static TimeOffset operator +(long a, TimeOffset b) => new(a + b.ms);
        public static long operator -(TimeOffset a, TimeOffset b) => a.ms - b.ms;
        public static TimeOffset operator -(long a, TimeOffset b) => new(a - b.ms);
        public static TimeOffset operator -(TimeOffset a, long b) => new(a.ms - b);
        public static bool operator >(TimeOffset a, TimeOffset b) => a.ms > b.ms;
        public static bool operator <(TimeOffset a, TimeOffset b) => a.ms < b.ms;
        public static bool operator <=(TimeOffset a, TimeOffset b) => a.ms <= b.ms;
        public static bool operator >=(TimeOffset a, TimeOffset b) => a.ms >= b.ms;
        public static bool operator ==(TimeOffset a, TimeOffset b) => a.ms == b.ms;
        public static bool operator !=(TimeOffset a, TimeOffset b) => a.ms != b.ms;

        public override readonly string ToString() =>
            DateTimeOffset.FromUnixTimeMilliseconds(ms).ToString("yyyy-MM-dd HH\\:mm\\:ss.fff", CultureInfo.InvariantCulture);
        public readonly string ToJSONString() =>
            DateTimeOffset.FromUnixTimeMilliseconds(ms).ToString("yyyy-MM-ddTHH\\:mm\\:ss.fffZ", CultureInfo.InvariantCulture);
        public readonly string ToCSVString() =>
            DateTimeOffset.FromUnixTimeMilliseconds(ms).ToString("yyyy-MM-dd HH\\:mm\\:ss", CultureInfo.InvariantCulture);
        public readonly string ToISOString() =>
            DateTimeOffset.FromUnixTimeMilliseconds(ms).ToString("yyyy-MM-ddTHH\\:mm\\:ss.fffZ", CultureInfo.InvariantCulture);
        public readonly string ToFSFriendlyString() =>
            DateTimeOffset.FromUnixTimeMilliseconds(ms).ToString("yyyy-MM-dd_HH-mm-ss", CultureInfo.InvariantCulture);
        public readonly string ToDateString() =>
            DateTimeOffset.FromUnixTimeMilliseconds(ms).ToString("yyyy-MM-dd", CultureInfo.InvariantCulture);

        public override readonly bool Equals(object obj) => obj is TimeOffset to && this == to;

        public override readonly int GetHashCode() => HashCode.Combine(ms);

        public readonly void WriteBinary(BinaryWriter bw)
        {
            int lower = (int)(ms & 0xFFFF_FFFF);
            short higher = (short)(ms >> 32);
            bw.Write(lower);
            bw.Write(higher);
        }

        public static TimeOffset ReadBinary(BinaryReader br)
        {
            ulong lower = br.ReadUInt32();
            ulong higher = br.ReadUInt16();
            return new((long)(lower | (higher << 32)));
        }
    }
}
