namespace SampleCode
{
    public interface ITickIndicator
    {
        public record struct TickValue(TimeOffset Time, int Size, double Price);

        TimeOffset LastTime { get; }
        double Value { get; }
        void AddTick(TickValue tick);
        double Recalculate(TimeOffset now);
    }
}