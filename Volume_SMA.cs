﻿using System;
using System.Collections.Generic;

namespace SampleCode
{
    public class Volume_SMA : ITickIndicator
    {
        public readonly int Seconds;
        protected Queue<ITickIndicator.TickValue> _queue = new();
        protected long _sum = 0;

        public Volume_SMA(int seconds)
        {
            Seconds = seconds;
        }

        public TimeOffset LastTime { get; protected set; }
        public double Value { get; protected set; }

        public void AddTick(ITickIndicator.TickValue tick)
        {
            if (tick.Time < LastTime)
                throw new ApplicationException($"Tick out of order!");
            Enqueue(tick);
            Recalculate(tick.Time);
        }

        public virtual double Recalculate(TimeOffset now)
        {
            Expire(now);
            Value = (double)_sum / Seconds;
            return Value;
        }

        protected virtual void Enqueue(ITickIndicator.TickValue tick)
        {
            _queue.Enqueue(tick);
            _sum += tick.Size;
        }
        protected virtual ITickIndicator.TickValue Dequeue()
        {
            var removed = _queue.Dequeue();
            _sum -= removed.Size;
            return removed;
        }

        protected void Expire(TimeOffset now)
        {
            var expiryTime = now.AddSeconds(-Seconds);
            while (_queue.Count > 0 && _queue.Peek().Time < expiryTime)
            {
                Dequeue();
            }
            LastTime = now;
        }
    }
}
